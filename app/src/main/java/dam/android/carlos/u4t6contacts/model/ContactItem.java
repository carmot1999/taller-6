package dam.android.carlos.u4t6contacts.model;

public class ContactItem {

    public String id;
    public String nombre;
    public String telefono;
    public String imagen;
    public String lookup;
    public String raw;
    public String type;

    public ContactItem(String id, String nombre, String telefono, String imagen, String lookup, String raw, String type) {
        // TODO Ex2.1: Nuevos datos a mostrar
        this.id = id;
        this.nombre = nombre;
        this.telefono = telefono;
        this.imagen = imagen;
        this.lookup = lookup;
        this.raw = raw;
        this.type = type;
    }
}
