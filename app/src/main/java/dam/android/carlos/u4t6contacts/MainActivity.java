package dam.android.carlos.u4t6contacts;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener {
    MyContacts myContacts;
    RecyclerView recyclerView;

    private static String[] PERMISSIONS_CONTACTS = {Manifest.permission.READ_CONTACTS};
    private static final int REQUEST_CONTACTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();

        if (checkPermissions()) {
            setListAdapter();
        }

        // TODO Ex2.1: Al mover elementos de RecyclerView ocultamos el TextView de información.
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                TextView tvInfo = findViewById(R.id.tvInfo);
                tvInfo.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void setUI() {

        recyclerView = findViewById(R.id.recyclerViewContacts);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
    }

    private void setListAdapter() {
        myContacts = new MyContacts(this);

        recyclerView.setAdapter(new MyAdapter(myContacts, this));

        if (myContacts.getCount() > 0) findViewById(R.id.tvEmptyList).setVisibility(View.INVISIBLE);
    }

    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, MainActivity.PERMISSIONS_CONTACTS, MainActivity.REQUEST_CONTACTS);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == REQUEST_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setListAdapter();
            } else {
                Toast.makeText(this, getString(R.string.contacts_read_right_required), Toast.LENGTH_LONG).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onItemClick(String id, String nombre, String telefono, String imagen, String lookup, String raw, String type) {
        TextView tvInfo = findViewById(R.id.tvInfo);

        String tipo = null;
        switch (type) {
            case "1":
                tipo = "HOME";
                break;
            case "2":
                tipo = "MOBILE";
                break;
            case "3":
                tipo = "WORK";
                break;
                default:
                    tipo = "UNKNOWN";
        }

        tvInfo.setText("CONTACT_ID: " + id + "\nLOOKUP_KEY: " + lookup + "\nRAW_CONTACT_ID: " + raw + "\nPHONE TYPE: " + tipo + "\nDISPLAY_NAME: " + nombre + "\nNUMBER: " + telefono + "\nPHOTO_THUMBNAIL_URI: " + imagen);
        tvInfo.setVisibility(View.VISIBLE);
    }

}
