package dam.android.carlos.u4t6contacts;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

import java.util.ArrayList;

import dam.android.carlos.u4t6contacts.model.ContactItem;

public class MyContacts {

    private ArrayList<ContactItem> myDataSet;
    private Context context;

    public MyContacts(Context context) {
        this.context = context;
        this.myDataSet = getContacts();
    }

    public ArrayList<ContactItem> getContacts() {
        ArrayList<ContactItem> contactsList = new ArrayList<>();

        ContentResolver contentResolver = context.getContentResolver();

        String[] projection = new String[] {
                // TODO Ex2.1: Datos a mostrar.
                ContactsContract.Data._ID,
                ContactsContract.Data.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.RawContacts.CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.LOOKUP_KEY,
                ContactsContract.Data.RAW_CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.TYPE,
                ContactsContract.Contacts.PHOTO_THUMBNAIL_URI};

        String selectionFilter = ContactsContract.Data.MIMETYPE + "='" +
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "' AND " +
                ContactsContract.CommonDataKinds.Phone.NUMBER + " IS NOT NULL";

        Cursor contactsCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI,
                projection,
                selectionFilter,
                null,
                ContactsContract.Data.DISPLAY_NAME + " ASC");

        if (contactsCursor != null) {

            // TODO Ex2.1: Datos a mostrar.
            int nameIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.DISPLAY_NAME);
            int numberIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int idIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.RawContacts.CONTACT_ID);
            int lookupIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.LOOKUP_KEY);
            int rawIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.RAW_CONTACT_ID);
            int typeIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.TYPE);
            int thumbnailIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI);

            while (contactsCursor.moveToNext()) {
                String name = contactsCursor.getString(nameIndex);
                String number = contactsCursor.getString(numberIndex);
                String id = contactsCursor.getString(idIndex);
                String lookup = contactsCursor.getString(lookupIndex);
                String raw = contactsCursor.getString(rawIndex);
                String type = contactsCursor.getString(typeIndex);
                String thumbnail = contactsCursor.getString(thumbnailIndex);

                // TODO Ex2.1: Nuevos datos a mostrar
                contactsList.add(new ContactItem(id, name, number, thumbnail, lookup, raw, type));
            }
            contactsCursor.close();
        }

        return contactsList;
    }

    public ContactItem getContactData(int position) {
        return myDataSet.get(position);
    }

    public int getCount() {
        return myDataSet.size();
    }

}
