package dam.android.carlos.u4t6contacts;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import dam.android.carlos.u4t6contacts.model.ContactItem;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private MyContacts myContacts;

    MyAdapter(MyContacts myContacts) {
        this.myContacts = myContacts;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        ImageView imagen;
        TextView id;
        TextView nombre;
        TextView telefono;

        public MyViewHolder(View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            imagen = itemView.findViewById(R.id.imagen);
            id = itemView.findViewById(R.id.id);
            nombre = itemView.findViewById(R.id.nombre);
            telefono = itemView.findViewById(R.id.telefono);
        }

        public void bind(final ContactItem activityName) {

        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {

        // TODO Ex1.2: Si el contacto tiene imagen, la mostramos.
        if (myContacts.getContactData(position).imagen != null) {
            viewHolder.imagen.setImageURI(Uri.parse(myContacts.getContactData(position).imagen));
        }
        viewHolder.id.setText(myContacts.getContactData(position).id);
        viewHolder.nombre.setText(myContacts.getContactData(position).nombre);
        viewHolder.telefono.setText(myContacts.getContactData(position).telefono);

        viewHolder.bind(myContacts.getContactData(position));
    }

    @Override
    public int getItemCount() {
        return myContacts.getCount();
    }

}
