package dam.android.carlos.u4t6contacts.model;

public class ContactItem {

    public String id;
    public String nombre;
    public String telefono;
    public String imagen;

    public ContactItem(String id, String nombre, String telefono, String imagen) {
        this.id = id;
        this.nombre = nombre;
        this.telefono = telefono;
        this.imagen = imagen;
    }
}
